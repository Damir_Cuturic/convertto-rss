<#
.SYNOPSIS
    Provides the RRS feed based on ITUNES ID
.DESCRIPTION
    Provides the RRS feed based on ITUNES ID
    https://itunes.apple.com/us/podcast/reply-all/idXXXXXXXXX
    use the numbers after the id
.EXAMPLE
    ItunesId-ToRSS "941907967"
    http://feeds.gimletmedia.com/hearreplyall
.INPUTS
    System String
.OUTPUTS
    String to the rrs link for the podcast
#>
function Itunes-IDtoRSS {
    [cmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [System.String]$id
    )
    $url =  "https://itunes.apple.com/lookup?id="+$id+"&entity=podcast"
    $response = Invoke-WebRequest -Uri $url
    $results = ConvertFrom-Json $response.Content
    Set-Clipboard $results.results.feedUrl
    $results.results.feedUrl
}

<#
.SYNOPSIS
    Provides the RRS feed based on ITUNES podcast url
.DESCRIPTION
    Provides the RRS feed based on ITUNES URL
    https://itunes.apple.com/us/podcast/reply-all/idXXXXXXXXX
.EXAMPLE
    ItunesTo-RSS "https://itunes.apple.com/nl/podcast/reply-all/id941907967"
    http://feeds.gimletmedia.com/hearreplyall
.INPUTS
    System String
.OUTPUTS
    String to the rrs link for the podcast
#>
function Itunes-UrltoRSS {
    [cmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [System.String]$url
    )
    $UrlID = ($url -split {$_ -eq "/" -or $_ -eq "?"})[6]
    $id = ($urlid -split "id" )[1]
    $requestURL = "https://itunes.apple.com/lookup?id="+$id+"&entity=podcast"
    $response = Invoke-WebRequest -Uri $requestURL
    $results = ConvertFrom-Json $response.Content
    Set-Clipboard $results.results.feedUrl
    $results.results.feedUrl
}